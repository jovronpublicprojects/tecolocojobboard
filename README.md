# Description
The solution have two NetCore 3.0 projects, one of them it's the WebApp Client and the another one it's the RestApi(backend)

# Configuration Steps
 In the RestApi configuration file (appsettings.json) make sure that your SQL Server connection string it's correct

Note: If you don't have the EFCore Packages to do the migration, you need to install the next components
```sh
PM> Install-Package Microsoft.EntityFrameworkCore.Tools
PM> Install-Package Microsoft.EntityFrameworkCore.Design
```

The solution have a code first migration with EF Core, you need to execute two command from the Project Console Managament
```sh
PM> Add-Migration
PM> Update-Migration
```
The front end need connection to the RestApi, make sure that parameter have the correctly URL (paramName: ServiceUrl)