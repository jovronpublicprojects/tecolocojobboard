﻿using AutoMapper;
using Core.UseCases.Interfaces;
using Dto.Domains;
using Repositories.Database.Entities;
using Repositories.Database.Interfaces;
using System.Collections.Generic;

namespace Core.UseCases.Implementations
{
    public class JobUseCase : IJobUseCase
    {
        private readonly IJobRepository JobRepository;
        private readonly IMapper Mapper;

        public JobUseCase(IJobRepository JobRepository, IMapper Mapper)
        {
            this.JobRepository = JobRepository;
            this.Mapper = Mapper;
        }

        public void Delete(int JobId)
        {
            var dto = Get(JobId);
            var entity = Mapper.Map<JobEntity>(dto); 
            JobRepository.Delete(entity);
        }

        public JobDto Get(int JobId)
        {
            return JobRepository.FindByPredicate<JobDto>(x => x.JobId.Equals(JobId), z => new JobDto()
            {
                JobId = z.JobId,
                Job = z.Job,
                JobTitle = z.JobTitle,
                Description = z.Description,
                CreatedAt = z.CreatedAt,
                ExpiresAt = z.ExpiresAt
            });
        }

        public List<JobDto> GetAll()
        {
            return JobRepository.FindByPredicateList<JobDto>(x => x.JobId > 0, z => new JobDto()
            {
                JobId = z.JobId,
                Job = z.Job,
                JobTitle = z.JobTitle,
                Description = z.Description,
                CreatedAt = z.CreatedAt,
                ExpiresAt = z.ExpiresAt
            });
        }

        public JobDto Save(JobDto Job)
        {
            var map = Mapper.Map<JobEntity>(Job);
            var entity = JobRepository.Save(map);
            var result = Mapper.Map<JobDto>(entity);
            return result;
        }

        public JobDto Update(JobDto Job)
        {
            var map = Mapper.Map<JobEntity>(Job);
            var entity = JobRepository.Update(map);
            var result = Mapper.Map<JobDto>(entity);
            return result;
        }
    }
}
