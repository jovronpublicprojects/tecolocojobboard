﻿using Dto.Domains;
using System.Collections.Generic;

namespace Core.UseCases.Interfaces
{
    public interface IJobUseCase
    {
        JobDto Get(int JobId);
        List<JobDto> GetAll();
        JobDto Save(JobDto Job);
        JobDto Update(JobDto Job);
        void Delete(int JobId);
    }
}
