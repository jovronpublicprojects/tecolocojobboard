﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Repositories.Database.Db
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        private readonly SqlDbContext DbContext;

        public GenericRepository(SqlDbContext DbContext)
        {
            this.DbContext = DbContext;
        }

        public void Delete(T Entity)
        {
            DbContext.Set<T>().Remove(Entity);
            DbContext.SaveChanges();
        }

        public void DisposeDb()
        {
            DbContext.Dispose();
        }

        public W FindByPredicate<W>(Expression<Func<T, bool>> Predicate, Expression<Func<T, W>> Include)
        {
            var Result = DbContext.Set< T > ().Where(Predicate).Select(Include).FirstOrDefault();
            return Result;
        }

        public List<W> FindByPredicateList<W>(Expression<Func<T, bool>> Predicate, Expression<Func<T, W>> Include)
        {
            var Result = DbContext.Set<T>().Where(Predicate).Select(Include).ToList();
            return Result;
        }

        public T Save(T Entity)
        {
            DbContext.Set<T>().Add(Entity);
            DbContext.SaveChanges();
            return Entity;
        }

        public T Update(T Entity)
        {
            DbContext.Set<T>().Update(Entity);
            DbContext.SaveChanges();
            return Entity;
        }
    }
}
