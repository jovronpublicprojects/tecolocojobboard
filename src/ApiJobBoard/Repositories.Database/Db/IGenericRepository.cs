﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Repositories.Database.Db
{
    public interface IGenericRepository<T> where T:class
    {
        T Save(T Entity);
        T Update(T Entity);
        W FindByPredicate<W>(Expression<Func<T,bool>> Predicate, Expression<Func<T,W>> Include);
        List<W> FindByPredicateList<W>(Expression<Func<T, bool>> Predicate, Expression<Func<T, W>> Include);
        void Delete(T Entity);
        void DisposeDb();
    }
}
