﻿using Microsoft.EntityFrameworkCore;
using Repositories.Database.Entities;


namespace Repositories.Database.Db
{
    public class SqlDbContext : DbContext
    {
        public SqlDbContext(DbContextOptions<SqlDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<JobEntity>().HasKey(x => x.JobId);
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<JobEntity> Jobs { get; set; }
    }
}
