﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Repositories.Database.Entities
{
    [Table(name: "TblJobs", Schema = "dbo")]
    public class JobEntity
    {

        [Column("JobId"), Key] public int JobId { get; set; }
        [Column("Job")] public string Job { get; set; }
        [Column("JobTitle")] public string JobTitle { get; set; }
        [Column("Description")] public string Description { get; set; }
        [Column("CreatedAt")] public DateTime CreatedAt { get; set; }
        [Column("ExpiresAt")] public DateTime? ExpiresAt { get; set; }
    }
}
