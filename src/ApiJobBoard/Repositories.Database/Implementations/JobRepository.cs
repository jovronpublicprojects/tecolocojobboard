﻿using Repositories.Database.Db;
using Repositories.Database.Entities;
using Repositories.Database.Interfaces;

namespace Repositories.Database.Implementations
{
    public class JobRepository : GenericRepository<JobEntity>, IJobRepository
    {
        public JobRepository(SqlDbContext DbContext) : base(DbContext)
        {
        }
    }
}
