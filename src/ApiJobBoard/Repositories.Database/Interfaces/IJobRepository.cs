﻿using Repositories.Database.Db;
using Repositories.Database.Entities;

namespace Repositories.Database.Interfaces
{
    public interface IJobRepository : IGenericRepository<JobEntity>
    {
    }
}
