﻿using Core.UseCases.Interfaces;
using Dto.Domains;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace RestApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class JobController : ControllerBase
    {
        private readonly IJobUseCase JobUseCase;

        public JobController(IJobUseCase JobUseCase)
        {
            this.JobUseCase = JobUseCase;
        }

        [HttpGet("Index")]
        public GenericResponse<string> Index()
        {
            GenericResponse<string> response = new GenericResponse<string>();

            response.Item = "Welcome";
            response.Status = "SUCCESS";
            response.Message = "RestApi";

            return response;
        }

        [HttpGet("Get/{JobId}")]
        public GenericResponse<JobDto> Get(int JobId)
        {
            GenericResponse<JobDto> response = new GenericResponse<JobDto>();
            try {
                response.Item = JobUseCase.Get(JobId);
                response.Status = "SUCCESS";
            } catch (Exception ex) {
                response.Item = null;
                response.Message = $"Message: {ex.Message} Inner: {ex.InnerException}";
                response.Status = "FAILURE";
            }

            return response;
        }

        [HttpGet("GetAll")]
        public GenericResponse<List<JobDto>> GetAll()
        {
            GenericResponse<List<JobDto>> response = new GenericResponse<List<JobDto>>();
            try
            {
                response.Item = JobUseCase.GetAll();
                response.Status = "SUCCESS";
            }
            catch (Exception ex)
            {
                response.Item = null;
                response.Message = $"Message: {ex.Message} Inner: {ex.InnerException}";
                response.Status = "FAILURE";
            }

            return response;
        }

        [HttpPost("Save")]
        public GenericResponse<JobDto> Save(JobDto Job)
        {
            GenericResponse<JobDto> response = new GenericResponse<JobDto>();
            try
            {
                response.Item = JobUseCase.Save(Job);
                response.Status = "SUCCESS";
            }
            catch (Exception ex)
            {
                response.Item = null;
                response.Message = $"Message: {ex.Message} Inner: {ex.InnerException}";
                response.Status = "FAILURE";
            }

            return response;
        }

        [HttpPost("Update")]
        public GenericResponse<JobDto> Update(JobDto Job)
        {
            GenericResponse<JobDto> response = new GenericResponse<JobDto>();
            try
            {
                response.Item = JobUseCase.Update(Job);
                response.Status = "SUCCESS";
            }
            catch (Exception ex)
            {
                response.Item = null;
                response.Message = $"Message: {ex.Message} Inner: {ex.InnerException}";
                response.Status = "FAILURE";
            }

            return response;
        }

        [HttpPost("Delete")]
        public GenericResponse<int> Delete(JobDto Job)
        {
            GenericResponse<int> response = new GenericResponse<int>();
            try
            {
                JobUseCase.Delete(Job.JobId);
                response.Item = 1;
                response.Status = "SUCCESS";
            }
            catch (Exception ex)
            {
                response.Item = 0;
                response.Message = $"Message: {ex.Message} Inner: {ex.InnerException}";
                response.Status = "FAILURE";
            }

            return response;
        }
    }
}