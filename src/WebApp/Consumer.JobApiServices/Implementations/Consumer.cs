﻿using Consumer.JobApiServices.Interfaces;
using Dto.Consumers;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Text;

namespace Consumer.JobApiServices.Implementations
{
    public class Consumer : IConsumer
    {
        public Consumer() { }

        public GenericResponse<T> Get<T>(string baseUrl, string controller, string action, string urlParameter)
        {
            GenericResponse<T> Response = new GenericResponse<T>();
            HttpClient Client;
            HttpRequestMessage requestMessage;
            HttpResponseMessage responseMessage;
            string stringResponse;
            try {
                Client = new HttpClient();
                Client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");

                if (string.IsNullOrEmpty(urlParameter)) {
                    requestMessage = new HttpRequestMessage(HttpMethod.Get, $"{baseUrl}/api/{controller}/{action}");
                }
                else
                {
                    requestMessage = new HttpRequestMessage(HttpMethod.Get, $"{baseUrl}/api/{controller}/{action}/{urlParameter}");
                }
                
                responseMessage = Client.SendAsync(requestMessage).Result;
                stringResponse = responseMessage.Content.ReadAsStringAsync().Result;
                Response = JsonConvert.DeserializeObject<GenericResponse<T>>(stringResponse);
            }
            catch (Exception ex)
            {
                Response.Message = $"Messge: {ex.Message} Inner: {ex.InnerException}";
                Response.Status = "FAILURE";
            } 

            return Response;
        }

        

        public GenericResponse<T> PostWithJson<T, W>(string baseUrl, string controller, string action, W Request)
        {
            GenericResponse<T> Response = new GenericResponse<T>();
            HttpClient Client;
            HttpRequestMessage requestMessage;
            HttpResponseMessage responseMessage;
            string stringResponse;
            try
            {
                Client = new HttpClient();
                Client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");
                requestMessage = new HttpRequestMessage(HttpMethod.Post, $"{baseUrl}/api/{controller}/{action}");
                requestMessage.Content = new StringContent(JsonConvert.SerializeObject(Request), Encoding.UTF8, "application/json");
                responseMessage = Client.SendAsync(requestMessage).Result;
                stringResponse = responseMessage.Content.ReadAsStringAsync().Result;
                Response = JsonConvert.DeserializeObject<GenericResponse<T>>(stringResponse);
            }
            catch (Exception ex)
            {
                Response.Message = $"Messge: {ex.Message} Inner: {ex.InnerException}";
                Response.Status = "FAILURE";
            }

            return Response;
        }
    }
}
