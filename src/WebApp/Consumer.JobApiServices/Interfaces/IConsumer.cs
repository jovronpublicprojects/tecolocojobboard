﻿using Dto.Consumers;

namespace Consumer.JobApiServices.Interfaces
{
    public interface IConsumer
    {
        GenericResponse<T> Get<T>(string baseUrl, string controller, string action, string urlParameter);
        GenericResponse<T> PostWithJson<T,W>(string baseUrl, string controller, string action, W Request);
    }
}
