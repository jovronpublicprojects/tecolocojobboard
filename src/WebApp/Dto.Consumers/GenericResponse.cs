﻿namespace Dto.Consumers
{
    public class GenericResponse<T>
    {
        public T Item { get; set; }
        public string Message { get; set; }
        public string Status { get; set; }
    }
}
