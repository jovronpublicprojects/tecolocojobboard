﻿using System;

namespace Dto.Consumers
{
    public class JobDto
    {
        public int JobId { get; set; }
        public string Job { get; set; }
        public string JobTitle { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? ExpiresAt { get; set; }
    }
}
