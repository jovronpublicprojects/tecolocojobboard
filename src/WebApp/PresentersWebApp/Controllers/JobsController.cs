﻿using Consumer.JobApiServices.Interfaces;
using Dto.Consumers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;

namespace PresentersWebApp.Controllers
{
    public class JobsController : Controller
    {
        private readonly IConsumer Consumer;
        private readonly IConfiguration Configuration;
        public JobsController(IConsumer Consumer, IConfiguration Configuration) {
            this.Consumer = Consumer;
            this.Configuration = Configuration;
        }

        public IActionResult Index()
        {
            GenericResponse<List<JobDto>> response = new GenericResponse<List<JobDto>>() ;
            try {
                string baseUrl = Configuration.GetValue<string>("ServiceUrl");
                response = Consumer.Get<List<JobDto>>(baseUrl, "Job", "GetAll", string.Empty);
            } catch (Exception ex) {
                response.Item = new List<JobDto>();
                response.Message = ex.Message;
                response.Status = "FAILURE";
            }
            
            return View(response.Item);
        }

        [HttpGet("Edit")]
        public IActionResult Edit(int Id)
        {
            GenericResponse<JobDto> response = new GenericResponse<JobDto>();
            ViewBag.Message = string.Empty;
            ViewBag.ErrorMessage = string.Empty;
            try
            {
                string baseUrl = Configuration.GetValue<string>("ServiceUrl");
                response = Consumer.Get<JobDto>(baseUrl, "Job", "Get", Id.ToString());
            }
            catch (Exception ex)
            {
                response.Item = new JobDto();
                response.Message = ex.Message;
                response.Status = "FAILURE";
            }

            return View(response.Item);
        }

        [HttpPost("Edit")]
        public IActionResult EditPost(JobDto data)
        {
            GenericResponse<JobDto> response = new GenericResponse<JobDto>();
            try
            {
                if (string.IsNullOrEmpty(data.Job))
                {
                    ViewBag.Message = string.Empty;
                    ViewBag.ErrorMessage = "The Job can't be empty";
                    return View("Edit", data);
                }

                string baseUrl = Configuration.GetValue<string>("ServiceUrl");
                response = Consumer.PostWithJson<JobDto, JobDto>(baseUrl, "Job", "Update", data);
                ViewBag.Message = "It's successful edited";
                ViewBag.ErrorMessage = string.Empty;
            }
            catch (Exception ex)
            {
                response.Item = new JobDto();
                response.Message = ex.Message;
                response.Status = "FAILURE";
                ViewBag.ErrorMessage = ex.Message;
                ViewBag.Message = string.Empty;
            }
            
            return View("Edit",response.Item);
        }

        [HttpGet("Create")]
        public IActionResult Create()
        {
            ViewBag.Message = string.Empty;
            ViewBag.ErrorMessage = string.Empty;
            return View("Create", new JobDto() { 
                CreatedAt = DateTime.Now
            });
        }

        [HttpPost("Create")]
        public IActionResult CreatePost(JobDto data)
        {
            GenericResponse<JobDto> response = new GenericResponse<JobDto>();
            try
            {
                if (string.IsNullOrEmpty(data.Job))
                {
                    ViewBag.Message = string.Empty;
                    ViewBag.ErrorMessage = "The Job can't be empty";
                    return View("Create", data);
                }

                string baseUrl = Configuration.GetValue<string>("ServiceUrl");
                response = Consumer.PostWithJson<JobDto, JobDto>(baseUrl, "Job", "Save", data);
                ViewBag.Message = "It's successful created";
                ViewBag.ErrorMessage = string.Empty;
            }
            catch (Exception ex)
            {
                response.Item = new JobDto();
                response.Message = ex.Message;
                response.Status = "FAILURE";
                ViewBag["ErrorMessage"] = ex.Message;
                ViewBag["Message"] = string.Empty;
            }            

            return View("Create", response.Item);
        }

        [HttpPost("Delete")]
        public IActionResult Delete(int JobId)
        {
            GenericResponse<JobDto> deleteResponse = new GenericResponse<JobDto>();
            try
            {

                string baseUrl = Configuration.GetValue<string>("ServiceUrl");
                deleteResponse = Consumer.PostWithJson<JobDto, JobDto>(baseUrl, "Job", "Delete", new JobDto() { JobId = JobId });
            }
            catch (Exception ex)
            {
                deleteResponse.Item = new JobDto();
                deleteResponse.Message = ex.Message;
                deleteResponse.Status = "FAILURE";
            }

            GenericResponse<List<JobDto>> response = new GenericResponse<List<JobDto>>();
            try
            {
                string baseUrl = Configuration.GetValue<string>("ServiceUrl");
                response = Consumer.Get<List<JobDto>>(baseUrl, "Job", "GetAll", string.Empty);
            }
            catch (Exception ex)
            {
                response.Item = new List<JobDto>();
                response.Message = ex.Message;
                response.Status = "FAILURE";
            }

            return View("Index", response.Item);
        }
    }
}